__author__ = 'Myzael'

import os
import re

FIRST_LINE = re.compile(r'\w+:', re.UNICODE)
LINE = re.compile(r'^( [\w\.\-\d]+\s*: *"[\w \?!\-\'\.\\,$\[\]§\*¿¡\(\)/&…£:©;¶\|\+=%’—¤#«@<>(\\")•]*"\s*$)|(^\s*)$', re.UNICODE)
for filename in os.listdir(
        "C:\\Users\\Myzael\\Documents\\Paradox Interactive\\Europa Universalis IV\\mod\\MEIOUandTaxes\\localisation"):
    if(not filename.endswith('russian.yml') and not filename.startswith('text') and not filename.startswith('flavor_events_l_german.yml') and not filename.startswith('Dei Gratia - Text removed from Paradox localisation files') and not filename == 'languages.yml'):
        with open(os.path.join(
                "C:\\Users\\Myzael\\Documents\\Paradox Interactive\\Europa Universalis IV\\mod\\MEIOUandTaxes\\localisation",
                filename), encoding="utf-8") as f:

            for index, line in enumerate(f):
                if index != 0:
                    if not LINE.match(line):
                        print("error in file %s on line %d: %s" % (filename, index+1, line))
