mods = dict()
with open('C:/Users/Myzael/Desktop/modifiers.txt') as file:
    for line in file:
        key = line.split('=')[0].strip()
        value = line.split('=')[1].strip()
        if key not in mods:
            mods[key] = set()
        mods[key].add(value)

for key in mods:
    listed = ",".join(mods[key])
    print("%s,%s" % (key,listed))
