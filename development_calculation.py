import math

__author__ = 'Myzael'
template = '''{0}if = {{
{0}	limit = {{
{0}		NOT = {{ total_development = {1} }}
{0}	}}
{0}	set_variable = {{ which = ud{4} value = 0 }}
{0}{2}
{0}}}
{0}if = {{
{0}	limit = {{
{0}		total_development = {1}
{0}	}}
{0}	set_variable = {{ which = ud{4} value = 1 }}
{0}{3}
{0}}}'''


def tabs(indent):
    return '\t' * indent


MAX = 2**15


def limit(imin, imax):
    return imax == MAX * 2 or imin == 1


def print_block(imin, imax, nesting):
    # print('{}{}'.format(tabs(nesting), int(math.log2(MAX)) - nesting))
    if imax - imin == 1: return ''
    midpoint = int((imin + imax) / 2)
    return template.format(tabs(nesting),
                           midpoint,
                           print_block(imin, midpoint, nesting + 1),
                           print_block(midpoint, imax, nesting + 1),
                           int(math.log2(MAX)) - nesting)


with open('C:/Users/Myzael/Desktop/asdf.txt','w') as file:
    file.write(print_block(0, MAX, 0))