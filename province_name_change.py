__author__ = 'Myzael'

import os
import re

def swap():
    TAG = re.compile(r'([0-9A-Z]{3})', re.UNICODE)
    PCULTURE = re.compile(r'primary_culture\s*=\s*(\w+)', re.UNICODE)
    history_countries = "C:\\Users\\Myzael\\Documents\\Paradox Interactive\\Europa Universalis IV\\mod\\MEIOUandTaxes\\history\\countries"
    for filename in os.listdir(
            history_countries):
        tag = re.search(TAG, filename).group(1)
        with open("%s\\%s" % (history_countries, filename)) as hist_file:
            for line in hist_file.readlines():
                match = re.search(PCULTURE, line);
                if match:
                    culture = match.group(1)
                    print('%s %s' % (tag, culture))

def create():
    province_names = "C:\\Users\\Myzael\\Documents\\Paradox Interactive\\Europa Universalis IV\\mod\\MEIOUandTaxes\\localisation\\prov_names_l_english.yml"