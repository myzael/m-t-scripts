import itertools

__author__ = 'Myzael'

template ='''if = {{
	limit = {{
		has_country_flag = adm_{0[0]}
		has_country_flag = dip_{0[1]}
		has_country_flag = mil_{0[2]}
		}}
	define_ruler = {{
		name = "xxx"
		age = 18
		fixed = yes
		ADM = {0[0]}
		DIP = {0[1]}
		MIL = {0[2]}
		}}
	}}
'''
stats = [2,3,4,5,6]

for i in ((a,b,c) for a in stats for b in stats for c in stats):
    print(template.format(i))
    # print(i)