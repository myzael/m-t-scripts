__author__ = 'Myzael'

l = ['Almásy', 'Ambrózy', 'Apponyi', 'Andrássy', 'Alvinczy', 'Apor',
'Balassa', 'Balogh', 'Bánffy', 'Barkóczy', 'Bedik', 'Bocskay', 'Benyovszky', 'Bercsényi', 'Berényi', 'Bethlen', 'Báthory', 'Batthyány', 'Beleznay', 'Blagay', 'Babarczy', 'Babocsai', 'Bajzáth', 'Bakics', 'Bánhidy', 'Bebek', 'Bihari', 'Bakos',
'Csáky', 'Cseszneky', 'Cziráky', 'Czobor', 'Czupor', 'Csávossy', 'Czibak', 'Csatáry', 'Cserkiczy',
'Dessewffy', 'Damjanich', 'Dőry', 'Dercsényi', 'Dévay', 'Diószegi', 'Dobó', 'Dóczy', 'Drágffy', 'Draveczky', 'Duka',
'Esterházy', 'Erdődy', 'Erőss', 'Erdey', 'Ebergényi', 'Eperjesy', 'Eötvös',
'Fáy', 'Fodróczy', 'Forgách', 'Forray', 'Földváry', 'Fűzy', 'Festetics', 'Frangepán', 'Fejérváry', 'Fekete', 'Fiáth',
'Garay', 'Győry', 'Gyulai', 'Gyürky', 'Gaál', 'Gyurits',
'Hunyadi', 'Hadik', 'Horthy', 'Horváth', 'Hédervári', 'Hodicz', 'Hagymássy', 'Herczog', 'Hollaky', 'Huszár', 'Illésházy', 'Imreffy', 'Inczédi', 'Inkey', 'Istvánffy',
'Jaksich', 'Jászy', 'Jászai', 'Jeszenszky', 'Jeszenák', 'Jókay', 'Jurisics',
'Kállay', 'Kálnoky', 'Kanizsai', 'Karacsay', 'Keresztes', 'Kéry', 'Kornis', 'Kőszegi', 'Kinizsi', 'Károlyi', 'Knézich', 'Kaplai', 'Kövecsess', 'Kürthy',
'Lackovics', 'Losonci', 'Lahner', 'Lázár', 'Liszti', 'Lónyai', 'Lámfalussy', 'Lehoczky',
'Majláth', 'Mikes', 'Milványi', 'Medgyesi', 'Maholányi', 'Malakóczy', 'Malonyai', 'Máriássy', 'Maróti', 'Marczali', 'Mednyánszky', 'Merényi', 'Mészáros', 'Metzger', 'Mérey', 'Mocsonyi', 'Móricz', 'Meszlényi',
'Nádasdy', 'Niczky', 'Németh', 'Naláczy', 'Neszméry', 'Nyáry',
'Olnódi', 'Orczy', 'Ocskay', 'Ozorai',
'Pálffy', 'Pázmány', 'Péchy', 'Petheő', 'Petky', 'Pongrácz', 'Potocki', 'Polereczky', 'Pászthory', 'Perényi', 'Petrőczy', 'Pollák', 'Prónay', 'Pulszky',
'Rákóczi', 'Rozgonyi', 'Rattkay', 'Ráday', 'Révay', 'Reviczky', 'Rudnay', 'Radák', 'Radványszky', 'Ráskai', 'Raszler', 'Rónay', 'Rudnyánszky',
'Széchenyi', 'Sándor', 'Szilágyi', 'Serényi', 'Simonyi', 'Somogyi', 'Szapáry', 'Szapolyai', 'Szécsi', 'Székely', 'Szirmay', 'Szunyogh', 'Szigethy', 'Sághy', 'Solymosy', 'Surányi', 'Szalay', 'Szegedy', 'Szeleczky', 'Szepessy', 'Szörényi', 'Szveteney', 'Szemerey',
'Török', 'Teleki', 'Thököly', 'Tisza', 'Tolvay', 'Temesváry', 'Tarczaly', 'Tallián', 'Telegdi', 'Turóczy', 'Turszky',
'Ujfalusy', 'Ujváry',
'Viczay', 'Vécsey', 'Wass', 'Wesselényi', 'Wilczek', 'Vajay', 'Várady', 'Vecsey', 'Vetésy',
'Zrínyi', 'Zichy', 'Zselénszky', 'Zách']
from collections import defaultdict
d = defaultdict(list)
for name in l:
    if name not in d[name[0].lower()]:
        d[name[0].lower()].append(name)

for letter in sorted(d.keys()):
    print (sorted(d[letter]))
