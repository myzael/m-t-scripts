__author__ = 'Myzael'

prefix1 = """{0}_{1} = {{
	potential = {{
		{0} = {1}
		NOT = {{ {0} = {2} }}
	}}
	trigger = {{
		{0} = {1}
		NOT = {{ {0} = {2} }}
	}}
"""

prefix2 = """{0}_{1} = {{
	potential = {{
		{0} = {1}
	}}
	trigger = {{
		{0} = {1}
	}}
"""

modifier = """	{} = {:.4}
"""

postfix = """}

"""

mods = [
    (('technology_cost', -0.05), ('idea_cost', -0.025)),
    (('dip_tech_cost_modifier', -0.05),),
    (('mil_tech_cost_modifier', -0.05), ('artillery_cost', -0.02), ('infantry_cost', -0.02)),
    (('dip_tech_cost_modifier', -0.05), ('heavy_ship_cost', -0.03), ('light_ship_cost', -0.03), ('transport_cost', -0.03)),
    (('adm_tech_cost_modifier', -0.05),),
    (('stability_cost_modifier', -0.025), ('relations_decay_of_me', 0.05), ('prestige_decay', -0.002))
]
names = ['university','refinery','weapons','wharf','textile','fine_arts_academy']
locs = ['university','refinery','weapons manufactory','wharf','textile manufactory','fine arts academy']
plurals = ['universities','refineries','weapons manufactories','wharfs','textile manufactories','fine arts academies']
modmap = dict(zip(names, mods))
locmap = dict(zip(names, locs))
pluralmap = dict(zip(names, plurals))

q = 2 / 3
RANGE = 21


def func(a, n):
    return round(a * (1 - q ** n) / (1 - q), 4)


with open(
        'C:/Users/Myzael/Documents/Paradox Interactive/Europa Universalis IV/mod/MEIOUandTaxes/common/triggered_modifiers/manufactory_triggered_modifiers.txt',
        'w') as f:
    for name in modmap.keys():
        if name == 'wharf' or name == 'refinery':
            pass
        for i in range(1, RANGE):
            prefix = prefix1
            if i == RANGE-1:
                prefix = prefix2
            f.write(prefix.format(name, i, i + 1))
            for mod in modmap[name]:
                f.write(modifier.format(mod[0], func(mod[1], i)))
            f.write(postfix)

with open(
        'C:/Users/Myzael/Documents/Paradox Interactive/Europa Universalis IV/mod/MEIOUandTaxes/localisation/manufactory_modifiers_l_english.yml',
        'w') as f:
    f.write('l_english:\n')
    for name in modmap.keys():
        for i in range(1, RANGE):
            if i == 1:
                s = locmap[name]
            else:
                s = pluralmap[name]
            nbr = str(i)
            if i == RANGE-1:
                nbr = str(i) + '+'
            f.write(' {0}_{1}: "{2}'.format(name, i, nbr))
            f.write(' {0}"\n'.format(s))
            f.write(' desc_{0}_{1}: "'.format(name, i))
            f.write('We have {0} {1} working."\n'.format(nbr, s))