__author__ = 'Myzael'

adm = frozenset(['administrative',
                 'asceticism',
                 'ceremony',
                 'economic',
                 'expansion',
                 'global_empire',
                 'humanist',
                 'innovativeness',
                 'popular_religion',
                 'scholasticism',
                 'theology',
                 'empire',
                 'engineering'])
dip = frozenset(['exploration',
                 'free_trade',
                 'aristocracy',
                 'democracy',
                 'diplomatic',
                 'culture',
                 'eminence',
                 'spy',
                 'influence',
                 'mercantilism',
                 'merchant_marine',
                 'trade',
                 'plutocracy'])
mil = frozenset(['leadership',
                 'logistic',
                 'mercenary',
                 'naval',
                 'naval_leadership',
                 'naval_quality',
                 'professional_army',
                 'quality',
                 'quantity',
                 'standing_army',
                 'grand_army',
                 'grand_fleet'])

for a,b in [(adm,dip),(dip,mil),(mil,adm)]:
    for one in a:
        for two in b:
            print(one + '_' + two + '_policy')