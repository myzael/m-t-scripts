__author__ = 'Myzael'

debug = True

def main():
    vanilla_path = input("Input path to your game install or press enter to use default (C:\Program Files (x86)\Steam\SteamApps\common\Europa Universalis IV):\n")
    if not vanilla_path:
        vanilla_path = "C:\Program Files (x86)\Steam\SteamApps\common\Europa Universalis IV"
    if debug:
        print(vanilla_path)

    moddir_path = input("Input path to mod folder enter to use default ("+os.environ['HOMEDRIVE'] + os.environ['HOMEPATH'] + "\Documents\Paradox Interactive\Europa Universalis IV\mod):\n")
    if not moddir_path:
        moddir_path = os.environ['HOMEDRIVE'] + os.environ['HOMEPATH'] + "\Documents\Paradox Interactive\Europa Universalis IV\mod"
    if debug:
        print(moddir_path)


if __name__ == "__main__":
    import sys
    import os
    sys.exit(main())
