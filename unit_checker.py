def checkPIPS():
    tech = dict()

    def func(t):
        # return t + 3
        return int(t/2) + 6

    with open(
            'C:/Users/Myzael/Documents/Paradox Interactive/Europa Universalis IV/mod/MEIOUandTaxes/common/technologies/mil.txt',
            'r') as f:
        techCounter = -1
        for line in f:
            line = line.split('#')[0].strip()
            if 'enable' in line:
                split__strip = line.split('=')[1].strip()
                if split__strip in tech:
                    print ("duplicate: %s" % split__strip)
                tech[split__strip] = techCounter
            elif 'technology' in line:
                techCounter += 1

    errorcount = 0
    count = 0

    for unit in sorted(tech.keys()):
        if True: #unit.startswith('eastern_') or unit.startswith('muslim_') or unit.startswith('western_') or unit.startswith('ottoman_'):
            count += 1
            with open('C:/Users/Myzael/Documents/Paradox Interactive/Europa Universalis IV/mod/MEIOUandTaxes/common/units/%s.txt' % unit, 'r') as f:
                sum = 0
                art = False
                for line in f:
                    line = line.split('#')[0].strip()
                    if 'offensive_morale' in line or 'defensive_morale' in line or 'offensive_fire' in line or 'defensive_fire' in line or 'offensive_shock' in line or 'defensive_shock' in line:
                        sum += int(line.split('=')[1].strip())
                    if 'artillery' in line: art = True
                if sum != func(tech[unit]) and not art:
                    print ('unit: %s, tech: %s, has: %f, should: %f' % (unit, tech[unit], sum,func(tech[unit])))
                    errorcount += 1

    print (errorcount)
    print (len(tech))
    print (count)

if __name__ == '__main__':
    checkPIPS()